import discord
from redbot.core import commands
import os
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
import datetime

# The CLIENT_SECRETS_FILE variable specifies
# the name of a file that contains
# client_id and client_secret.
CLIENT_SECRETS_FILE = None

# This scope allows for full read/write access
# to the authenticated user's account and requires
# requests to use an SSL connection.
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

# ID of the playlist videos will be posted to.
playlistId = None

#todo make authorization process happen all in discord
#todo make it so you only have to authorize once using a refresh token

# datetime to keep track of last time addhist2p command was run
lastsetdatetime = None

class ytplaylistcog(commands.Cog):
    """This cog will add yt links to a yt playlist."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def setutcdatetime(self, ctx, arg):
        global lastsetdatetime

        lastsetdatetime = arg
        try:
            datetime.datetime.strptime(lastsetdatetime, '%Y-%m-%d %H:%M:%S.%f')
        except ValueError as error:
            await ctx.send("Does not match format: '%Y-%m-%d %H:%M:%S.%f'")
            return

        await ctx.send("UTC Datetime set!")
        return

    @commands.command()
    async def printutcdatetime(self, ctx):
        global lastsetdatetime
        if lastsetdatetime == "":
            await ctx.send("UTC Date time not set! Please run `[p]setutcdatetime`.")
            return
        elif lastsetdatetime is None:
            await ctx.send("UTC Date time not set! Please run `[p]setutcdatetime`.")
            return

        await ctx.send(lastsetdatetime)
        return

    @commands.command()
    async def setplaylistid(self, ctx, arg):
        global playlistId
        playlistId = arg
        await ctx.send("Playlist ID set!")
        return

    @commands.command()
    async def printplaylistid(self, ctx):
        global playlistId

        if playlistId == "":
            await ctx.send("PlaylistID not set! Please run `[p]setplaylistid`.")
            return
        elif playlistId is None:
            await ctx.send("PlaylistID not set! Please run `[p]setplaylistid`.")
            return

        await ctx.send(playlistId)
        return

    @commands.command()
    async def printclientfiledir(self, ctx):
        global CLIENT_SECRETS_FILE

        if CLIENT_SECRETS_FILE == "":
            await ctx.send("Client secrets file directory not set! Please run `[p]setclientfiledir`.")
            return
        elif CLIENT_SECRETS_FILE is None:
            await ctx.send("Client secrets file directory not set! Please run `[p]setclientfiledir`.")
            return

        await ctx.send(CLIENT_SECRETS_FILE)
        return

    @commands.command()
    async def setclientfiledir(self, ctx, arg):
        global CLIENT_SECRETS_FILE
        CLIENT_SECRETS_FILE = arg
        await ctx.send("Client secrets file directory set!")
        return

    async def getclientfiledir(self):
        global CLIENT_SECRETS_FILE
        return CLIENT_SECRETS_FILE

    async def getplaylistid(self):
        global playlistId
        return playlistId

    async def listofVidIDofMsgHistory(self, ctx):
        txtchan = ctx.channel
        lstofytvid = []

        # if this command has never been run, run it with limit=None and set
        # global lastsetdatetime to now after for loop finishes
        # else run command for messages after lastsetdatetime
        global lastsetdatetime

        if lastsetdatetime is None:
            async with ctx.channel.typing():
                async for message in txtchan.history(limit=None):
                    ytvidID: str = await self.getVideoID(ctx, message.content)
                    lstofytvid.append(ytvidID)
            lastsetdatetime = datetime.datetime.utcnow()
        else:
            async with ctx.channel.typing():
                async for message in txtchan.history(after=lastsetdatetime):
                    ytvidID: str = await self.getVideoID(ctx, message.content)
                    lstofytvid.append(ytvidID)

            lastsetdatetime = datetime.datetime.utcnow()

        return lstofytvid

    async def getauth(self, ctx):
        CSF = await self.getclientfiledir()
        if CSF is None:
            await ctx.send("You need to set the directory for your client_secrets_file using [p]setclientfiledir.")
            return
        flow = InstalledAppFlow.from_client_secrets_file(
            CSF, SCOPES)

        credentials = flow.run_console()
        return build(API_SERVICE_NAME, API_VERSION, credentials=credentials)

    async def getVideoID(self, ctx, arg: str):
        yt1: str = 'youtube.com'
        yt2: str = 'm.youtube.com'
        yt3: str = 'youtu.be'

        if yt1 in arg:
            head = arg.split('&')
            id = head[0].split('=')
            return id[1]
        elif yt2 in arg:
            head = arg.split('&')
            id = head[0].split('=')
            return id[1]
        elif yt3 in arg:
            head = arg.split('?')
            id = head[0].split('/')
            return id[3]
        else:
            return None

    @commands.command()
    async def addhist2p(self, ctx):
        """Add all youtube videos in the channel that were posted since last time the command was run
         to a youtube playlist."""

        # get oauth permission to edit playlist
        youtube = await self.getauth(ctx)

        # msghist method that gets list of all ytlinks that have ever been posted
        lstofvidIDs: list = await self.listofVidIDofMsgHistory(ctx)
        plID = await self.getplaylistid()
        if plID is None:
            await ctx.send('You need to set the playlist ID using [p]setplaylistid.')
            return

        for vidID in lstofvidIDs:
            if vidID is not None:
                request = youtube.playlistItems().insert(
                    part="snippet",
                    body={
                        "snippet": {
                            "playlistId": plID,
                            "position": 0,
                            "resourceId": {
                                "kind": "youtube#video",
                                "videoId": vidID
                            }
                        }
                    }
                )
                request.execute()

        await ctx.send("Videos added successfully!")

    @commands.command()
    async def addv2p(self, ctx, arg):
        """Add a single youtube video to a youtube playlist. Format: [p]addv2p <ytlink>"""

        # get video ID from yt link
        vidID: str = await self.getVideoID(ctx, arg)

        # return if message doesnt have a yt link in it
        if vidID is None:
            await ctx.send('Command doesn\'t have a yt link in it.')
            return

        # get oauth permission to edit playlist
        youtube = await self.getauth(ctx)
        plID = await self.getplaylistid()
        if plID is None:
            await ctx.send('You need to set the playlist ID using [p]setplaylistid.')
            return

        request = youtube.playlistItems().insert(
            part="snippet",
            body={
              "snippet": {
                "playlistId": plID,
                "position": 0,
                "resourceId": {
                  "kind": "youtube#video",
                  "videoId": vidID
                }
              }
            }
        )
        request.execute()
        await ctx.send("Video added successfully!")