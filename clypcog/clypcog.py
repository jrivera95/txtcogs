import discord

try:  # check if BeautifulSoup4 is installed
    from bs4 import BeautifulSoup

    soupAvailable = True
except:
    soupAvailable = False

from selenium import webdriver
from redbot.core import commands


class clypcog(commands.Cog):
    """This cog will play a clyp if you give it a valid clyp.it link."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def test(self, ctx, arg):
        await ctx.send(arg)

    @commands.command()
    async def clyp(self, ctx, arg):
        """Format for playing clyps: '[p]clyp <clyp.it link>'. You can also use the following in place of the clyp.it link: 'pause', 'resume', 'stop' and 'unjoin'. This cog requires you to have chromedriver.exe and ffmpeg.exe in your path directory. Alternatively you can install them using chocolatey and it'll work fine."""

        member = ctx.author

        lst: list = ctx.bot.voice_clients
        # implement stop, pause, resume and unjoin commands
        if arg == 'stop':
            if lst[0].is_playing():
                lst[0].stop()
                return
            else:
                await ctx.send("Can't stop if I'm not playing.")
                return
        elif arg == 'pause':
            if lst[0].is_playing():
                lst[0].pause()
                return
            else:
                await ctx.send("Can't pause if I'm not playing.")
                return
        elif arg == 'resume':
            if lst[0].is_paused():
                lst[0].resume()
                return
            else:
                await ctx.send("Can't resume if I'm not paused.")
                return
        elif arg == 'unjoin':
            try:
                await lst[0].disconnect(force=True)
                return
            except IndexError:
                await ctx.send("Can't unjoin if I'm not connected to a voice channel. ;^)")
                return

        # build the web address
        url = str(arg)
        clyp = 'https://clyp.it/'

        # check if url has the clyp address in it and that it is valid
        if clyp not in url or clyp == url:
            return await ctx.send("Use a valid link from clyp.it.")

        # set chrome options so it doesn't open
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('disable_gpu')
        try:
            driver = webdriver.Chrome('chromedriver', chrome_options=options)
        except:
            await ctx.send(
                'Please make sure you have the directory for \'chromedriver.exe\' in your PATH environment variable.')
            return
        # go to the page
        driver.get(url)

        # get inner html
        html = driver.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
        soup = BeautifulSoup(html, 'html.parser')

        # fetch src link and clyp name
        try:
            srclink = soup.audio['src']

        except KeyError:
            return await ctx.send("Invalid clyp.it link.")

        # instances a voice channel
        voicechannel = discord.VoiceChannel

        # says if opus is loaded
        test = discord.opus.is_loaded()
        if not test:
            await ctx.send("Opus library is not loaded.")
            return

        member = ctx.author
        # checks if there are any voice connections
        if not lst:
            # Bot isnt already in a voice channel.
            try:
                # connects to author's voice channel
                vc = await voicechannel.connect(member.voice.channel)

            except AttributeError:
                return await ctx.send("Connect to a voice channel first.")

            if vc.is_playing():
                await ctx.send("Please wait until the current clyp ends.")
            else:
                try:
                    vc.play(discord.FFmpegPCMAudio(srclink, executable='ffmpeg'), after=lambda e: print('done', e))
                except:
                    await ctx.send(
                        'Please make sure you have the directory for \'ffmpeg.exe\' in your PATH environment variable.')
                    return
        else:
            # Already connected to a voice channel.

            if lst[0].is_playing():
                await ctx.send("Please wait until the current clyp ends.")
            else:
                try:
                    lst[0].play(discord.FFmpegPCMAudio(srclink, executable='ffmpeg'), after=lambda e: print('done', e))
                except:
                    await ctx.send(
                        'Please make sure you have the directory for \'ffmpeg.exe\' in your PATH environment variable.')
                    return

        driver.quit()
