from .clypcog import clypcog
try: # check if BeautifulSoup4 is installed
	from bs4 import BeautifulSoup
	soupAvailable = True
except:
	soupAvailable = False


def setup(bot):
	if soupAvailable:
		bot.add_cog(clypcog(bot))
	else:
		raise RuntimeError("Beautifulsoup4 python package not installed. You need to run `pip3 install beautifulsoup4` to install it.")

