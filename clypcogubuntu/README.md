# Clypcogubuntu
These are the linux instructions of this cog; made to run on Ubuntu 18LTS. A cog for the [Red Discord Bot (V3)](https://github.com/Cog-Creators/Red-DiscordBot/).

# Setup:

1. Make sure you have python3 (I used 3.7) installed as well the required python packages installed: 'selenium', 'beautifulsoup4' and 'PyNaCl'.

2. You'll need to install the software library libsodium; for this you can run `sudo apt install libsodium23`.

3. Install google chrome and download the corresponding [chromedriver file](http://chromedriver.chromium.org/downloads), according to your google chrome version.
    Put the chromedriver file in a folder that is accessible from $PATH.

4. Install ffmpeg: just enter `sudo apt install ffmpeg` into Terminal. Version 3 of ffmpeg will do.

5. Make sure you can run `ffmpeg` and `chromedriver` as commands from Terminal in any directory (this means they are accessible from PATH).

6. Run this line of code in Terminal `sudo apt-get install -y unzip xvfb libxi6 libgconf-2-4`.

7. Download the folder `clypcogubuntu` into the path or directory where your installation of Red gets cogs.

8. In discord, type `[p]load clypcogubuntu`.

9. You can now play clyp.it links in VC!!!

Type and enter `[p]load clypcog` inside discord to load the cog. Then, for info on how to use it, simply type `[p]clyp`.

# Contact
Feel free to ping/DM me through discord at ✮~ Ｔ Ｘ Ｔ ~✮#9441 or raise an issue through here if you have any problems.