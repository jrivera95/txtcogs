import discord
from redbot.core import commands
import datetime
import time
import calendar
from discord.utils import escape_mentions
from redbot.core import Config


# original cog by rasi
# this is a conversion of the cog from V2 to V3

class challengecog(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.config = Config.get_conf(self, identifier=13243546576879809)
        default_global = {
            "challenge_time_date_theme": []
        }
        self.config.register_global(**default_global)

    @commands.command()
    @commands.admin_or_permissions()
    async def setchallenge(self, ctx, *, yyyy_mm_dd_hh_theme):
        """Sets challenges ending time and theme."""
        '''Usage: YYYY MM DD HH theme'''
        '''Set time is local'''
        # get the time and theme
        tmp = [escape_mentions(c) for c in yyyy_mm_dd_hh_theme]
        tmpy = escape_mentions(''.join(tmp[0:13]).replace(" ", ".").replace("/", ".").replace("_", "."))
        tmps = ''.join(tmp[14:len(tmp)])
        try:  # check for wrong format
            tmpt = time.strptime(tmpy, "%Y.%m.%d.%H")
            tmpt = time.localtime(time.mktime(tmpt))
            tmpy1 = time.strftime("%Y.%m.%d %H:%M:%S", tmpt)
            tnow = time.localtime()
            tnow1 = time.strftime("%Y.%m.%d %H:%M:%S", tnow)
            if time.mktime(tnow) > time.mktime(tmpt):
                await ctx.send("Challenge date is earlier then now " + tnow1 + ".")
            else:
                await ctx.send("Challenge set  at " + tnow1 + " " + time.tzname[0] + ".")
                await ctx.send("Challenge ends on  " + tmpy1 + " " + time.tzname[0] + ".")
                await ctx.send("Theme = " + tmps + ".")
                await self.config.challenge_time_date_theme.set(tmpy1 + tmps)
                # dataIO.save_json("data/customcom/chlng.json", tmpy1 + tmps)
        except ValueError:
            await ctx.send("Wrong format. Correct format: yyyy_mm_dd_hh_theme")

    @commands.command()
    @commands.admin_or_permissions()
    async def endchallenge(self, ctx):  # end challenge command
        """Deletes and stops the challenge."""
        await ctx.send("Challenge deleted.")
        await self.config.challenge_time_date_theme.set([])

    @commands.command()
    async def challenge(self, ctx):  # challenge command
        """Prints the deadline and theme of the challenge."""

        tmp = await self.config.challenge_time_date_theme()
        if not tmp or tmp == '':
            await ctx.send("Challenge not set. Please set the challenge time and theme using the `$setchallenge` command.")
        else:
            tmps = tmp[19:len(tmp)]
            tmp = tmp[0:18]
            tnow = time.localtime()
            tmpt = time.strptime(tmp, "%Y.%m.%d %H:%M:%S")
            tmpd = time.mktime(tmpt) - time.mktime(tnow)
            if tmpd < 0:
                await ctx.send("Last challenge ended " + tmp + ".")
            else:
                tmpd = datetime.timedelta(seconds=tmpd)
                await ctx.send("Challenge theme: " + tmps + ".")
                await ctx.send("Time left for submission " + str(tmpd) + ".")
