# Clypcog cog
A cog for [Red Discord Bot (V3)](https://github.com/Cog-Creators/Red-DiscordBot/).
These are the **windows** setup instructions of this cog. 
**The folder 'clypcogubuntu' contains setup instructions of the cog for ubuntu/linux.**

NOTE: To use this cog you must first do the following things:

1. Install the python packages 'selenium', 'beautifulsoup4' and 'PyNaCl' using the `python -m pip install <package name>` command.

2. (option 1) Install the google chrome browser and [ffmpeg](https://ffmpeg.org/).
    Download [chromedriver.exe](http://chromedriver.chromium.org/downloads).
    Add 'ffmpeg.exe' and 'chromedriver.exe' directories to your PATH environment variable.

OR

2. (option 2) If you use the [chocolatey windows pkg manager](https://chocolatey.org/), just enter `choco install ffmpeg` and `choco install chromedriver` into cmd and you should be good to go (you still need the python packages 'selenium' and 'beatifoulsoup4' of course).

# Installation
To install simply clone (download) this repository into your CogManager/cogs folder.
Your final folder structure should look like `/CogManager/cogs/clypcog`.

Type and enter `[p]load clypcog` inside discord to load the cog. Then, for info on how to use it, simply type `[p]clyp`.

# Contact
You can ping/DM me through discord at ✮~ Ｔ Ｘ Ｔ ~✮#9441 or raise an issue through here.